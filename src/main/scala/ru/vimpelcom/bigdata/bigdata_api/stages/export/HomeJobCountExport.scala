package ru.vimpelcom.bigdata.bigdata_api.stages.export

import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import ru.vimpelcom.bigdata.bigdata_api.system.Parameters

class HomeJobCountExport(spark: SparkSession, conf: Parameters) {

  val columns: Seq[String] = Seq(
    "ctn",
    "job_top_1_latitude",
    "job_top_1_longitude",
    "job_top_2_latitude",
    "job_top_2_longitude",
    "home_top_1_latitude",
    "home_top_1_longitude",
    "home_top_2_latitude",
    "home_top_2_longitude"
  )

  val noLocation = "Location_not_found"

  val homeJobTopsFiltered: DataFrame = spark.table(s"${conf.HIVE_DATABASE}.${conf.REPORT_TABLE}")
    .select(columns.map(col): _*)
    .where(col("time_key").equalTo("201910"))
    .where(
      not((col("job_top_1_latitude") like noLocation).or(col("job_top_1_longitude") like noLocation)) or
        not((col("job_top_2_latitude") like noLocation).or(col("job_top_2_longitude") like noLocation)) or
        not((col("home_top_1_latitude") like noLocation).or(col("home_top_1_longitude") like noLocation)) or
        not((col("home_top_2_latitude") like noLocation).or(col("home_top_2_longitude") like noLocation))
    )
    .repartition(conf.MAX_CONNECTIONS)
    .withColumn("job_top_1_latitude", col("job_top_1_latitude").cast(DataTypes.DoubleType))
    .withColumn("job_top_1_longitude", col("job_top_1_longitude").cast(DataTypes.DoubleType))
    .withColumn("job_top_2_latitude", col("job_top_2_latitude").cast(DataTypes.DoubleType))
    .withColumn("job_top_2_longitude", col("job_top_2_longitude").cast(DataTypes.DoubleType))
    .withColumn("home_top_1_latitude", col("home_top_1_latitude").cast(DataTypes.DoubleType))
    .withColumn("home_top_1_longitude", col("home_top_1_longitude").cast(DataTypes.DoubleType))
    .withColumn("home_top_2_latitude", col("home_top_2_latitude").cast(DataTypes.DoubleType))
    .withColumn("home_top_2_longitude", col("home_top_2_longitude").cast(DataTypes.DoubleType))

  homeJobTopsFiltered
    .write
    .format("jdbc")
    .mode(SaveMode.Overwrite)
    .options(conf.postgresConfig)
    .save()


}
