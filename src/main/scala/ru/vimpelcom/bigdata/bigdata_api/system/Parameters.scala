package ru.vimpelcom.bigdata.bigdata_api.system

import java.util.Properties

import org.apache.log4j.Logger
import org.apache.spark.sql.types._

class Parameters(args: Array[String])(implicit log: Logger) {
  val paramMap = args.map(argLine => argLine.split("=", -1) match {
    case Array(key, value) => key -> value
    case _ =>
      log.error(s"Cannot parse parameter string: $argLine")
      throw new Throwable(s"Cannot parse parameter string: $argLine")
  }).toMap

  log.info("===" * 5)
  log.info(paramMap.map {
    case (k,v) => s"$k = $v"
  }.mkString("\n"))
  log.info("===" * 5)

  val HIVE_DATABASE: String = paramMap.getOrElse("HIVE_DATABASE", "")
  private val POSTGRES_CONNECTION_STRING: String = paramMap.getOrElse("POSTGRES_CONNECTION_STRING", "") //jdbc:postgresql://localhost:5432/db
  val REPORT_TABLE: String = paramMap.getOrElse("REPORT_TABLE", "")
  private val POSTGRES_USER: String = paramMap.getOrElse("POSTGRES_USER", "")
  private val POSTGRESS_PASSWORD: String = paramMap.getOrElse("POSTGRESS_PASSWORD", "")
  val MAX_CONNECTIONS: Int = paramMap.getOrElse("MAX_CONNECTIONS", "1").toInt

  def postgresConfig: Map[String, String] = Map(
    "url" -> POSTGRES_CONNECTION_STRING,
    "driver" -> "org.postgresql.Driver",
    "dbtable" -> REPORT_TABLE,
    "user" -> POSTGRES_USER,
    "password" -> POSTGRESS_PASSWORD
  )

  def getPostgresProperties: Properties = {
    (new Properties /: postgresConfig) {
      case (prop, (key, value)) =>
        prop.put(key, value)
        prop
    }
  }


}