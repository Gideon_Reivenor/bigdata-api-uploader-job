package ru.vimpelcom.bigdata.bigdata_api.system

import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import ru.vimpelcom.bigdata.bigdata_api.stages.export.HomeJobCountExport

object Application {
  private implicit val log: Logger = Logger.getLogger(Application.getClass)

  def main(args: Array[String]): Unit = {
    Class.forName("org.postgresql.Driver")
    val parameters = new Parameters(args)
    val sparkConf = new SparkConf()
    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .config("hive.exec.dynamic.partition", "true")
      .config("hive.exec.dynamic.partition.mode", "nonstrict")
      .config("spark.sql.hive.convertMetastoreOrc", "true")
      .config("spark.sql.hive.verifyPartitionPath", "true")
      .enableHiveSupport()
      .getOrCreate()

    new HomeJobCountExport(spark, parameters)

    spark.stop()
  }
}
