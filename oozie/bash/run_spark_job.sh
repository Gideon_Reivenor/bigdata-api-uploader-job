#!/usr/bin/env bash

export SPARK_MAJOR_VERSION=2

spark-submit \
--class ru.vimpelcom.bigdata.bigdata_api.system.Application \
--master yarn-cluster \
--num-executors 10 \
--executor-cores 1 \
--driver-memory 4G \
--executor-memory 8G \
--conf spark.app.name=bigdata_api_uploader_job \
--conf spark.driver.extraClassPath=./postgresql-42.1.4.jar \
--conf spark.executor.extraClassPath=./postgresql-42.1.4.jar \
--queue ${QUEUE_NAME} \ #adhoc
${JAR_PATH} \ #geo-reporter-1.0-SNAPSHOT.jar
HIVE_DATABASE=${HIVE_DATABASE} \ #home_job
POSTGRES_CONNECTION_STRING=${POSTGRES_CONNECTION_STRING} \ #jdbc:postgresql://10.31.150.231:5431/geo_reports
POSTGRES_USER=${POSTGRES_USER} \ #postgres
POSTGRESS_PASSWORD=${POSTGRESS_PASSWORD} \
MAX_CONNECTIONS=${MAX_CONNECTIONS} \ #50
REPORT_TABLE=${REPORT_TABLE}; #home_job_m
